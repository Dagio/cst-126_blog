<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.6

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 3, 2017

Synopsis:
This file builds a MySQL query and inserts the new comment into the database.  Displays a message to the user indicating
success or failure.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Comment Posted</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<?php

echo "<br>";
include_once '_functions.php';

echo '
<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="linkStyle.css">
';
echo "<!-- Styling for the table-->
<link rel=\"stylesheet\" type=\"text/css\" href=\"tableStyle.css\">";

$mysqli = dbConnect();

$previousPage = "blogDisplay.php?blogID=".$_SESSION['$comments_blogs_fk'];

$commentEntry = $_POST['comment'];

if (empty($commentEntry)){
    error("Comment cannot be left blank.", $previousPage);
    include '_errorDbClose.php';
}

$insertQuery = "INSERT INTO comments (blogs_fk, users_fk, date_created, message) VALUES ((?), (?), (?), (?))";

$currentBlog = $_SESSION['$comments_blogs_fk'];
$currentUser = $_SESSION['user_id'];

// Get current date and time
// This will be stored in comments table as date_created
$commentDate = date('Y-m-d H:i:s');

// Prepared SQL Statement
if (!($preparedStatement = $mysqli -> prepare($insertQuery)))
{
    error("Could not prepare query.", $previousPage);
    include '_errorDbClose.php';
}

if (!$preparedStatement -> bind_param("ssss", $currentBlog, $currentUser, $commentDate, $commentEntry))
{
    error("Could not bind parameters.", $previousPage);
    include '_errorDbClose.php';
}

if (!$preparedStatement -> execute())
{
    error("Database error:  Could not create new user.", $previousPage);
    include '_errorDbClose.php';
}

$mysqli->close();

echo nl2br("Comment posted successfully.  Redirecting back to blog entry...");
echo nl2br('<meta http-equiv="refresh" content="2;URL=blogDisplay.php?blogID=' . $_SESSION['$comments_blogs_fk'] . '" />');

?>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>