<?php
/*
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
Page to call the session start, to be included at the beginning of each page to ensure that the session executes properly
*/

session_start();