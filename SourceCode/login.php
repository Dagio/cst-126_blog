<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This file contains the login retrieval information for users attempting to access the blog. Form data is sent to a
PHP handler where it is checked against the database to determine if the user is viable and then logs them in if so.
Additionally there is a register button for new users.

References:
HTML Input Types - https://www.w3schools.com/html/html_form_input_types.asp
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Login</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>

    <form class="standardForm" action="loginHandler.php" method="post">
        Username
        <input type="email" name="email" placeholder="Enter your email address" maxlength="100">
        Password
        <input type="password" name="password" placeholder="Enter your password" maxlength="100">
        <input type="submit" value="Login" class="button">
    </form>

    <form class="standardForm" action="register.php">
        <input type="submit" value="Register" class="button">
    </form>

</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>