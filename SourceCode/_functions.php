<?php
/*
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This file contains the custom functions used throughout this project.  The following line is required in each .php file
in order to use these functions:

include_once '_functions.php';
*/

function dbConnect()
{
    $host = "localhost";
    $user = "root";
    $pass = "root";
    $dbname = "blog";
    $port = 8889;

    //Reference to database connection
    $mysqli = new mysqli($host, $user, $pass, $dbname, $port);

    //Throw error is connection fails
    if ($mysqli->connect_errno) {
        error(nl2br("Could not connect to database.\n" . $mysqli->connect_error));
    }

    return $mysqli;
}

function error($errorMessage, $previousPage)
{
    // Store error message and previous URL in session
    // These values need to persist so they can be displayed on "error.php"
    $_SESSION['error_message'] = $errorMessage;
    $_SESSION['previous_page'] = $previousPage;
    echo '<meta http-equiv="refresh" content="0;URL=error.php" />';
}
function warning($warningMessage, $previousPage)
{
    // Store error message and previous URL in session
    // These values need to persist so they can be displayed on "warning.php"
    $_SESSION['warning_message'] = $warningMessage;
    $_SESSION['previous_page'] = $previousPage;
    echo '<meta http-equiv="refresh" content="0;URL=warning.php" />';
}
