<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
blogDisplay is where active blogs are displayed for the user to read, edit, or delete.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Display Blog</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>
<?php
include_once '_displayHandler.php';
include_once '_commentHandler.php';
?>
</body>
<footer>
    <?php
    include '_footer.php';
    ?>
</footer>
</html>
