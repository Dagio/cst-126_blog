<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.7

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 10, 2017

Synopsis:
blogDisplay is where active blogs are displayed for the user to read, edit, or delete.

References:
https://stackoverflow.com/questions/6017598/html-php-default-input-value
https://stackoverflow.com/questions/16263548/load-value-from-sql-table-in-html-textarea
-->

<?php
include_once '_functions.php';

//Fetch blog title from blog list
$blogID = $_GET['blogID'];
//Set up database connection
$mysqli = dbConnect();
//Query to select the entire entry associated with the blog title
$query = "SELECT * FROM blogs WHERE id=$blogID";
$result = mysqli_query($mysqli, $query);

// CSS
echo "<!-- Styling for the table-->
<link rel=\"stylesheet\" type=\"text/css\" href=\"tableStyle.css\">";

// Set the associated entry to a variable
$row = mysqli_fetch_array($result);

$authorID = $row['users_fk'];
$authorQuery = "SELECT * FROM users WHERE id=" . $authorID;
$authorResult = mysqli_query($mysqli, $authorQuery);

// Set the associated author to a variable
$authorRow = mysqli_fetch_array($authorResult);

// Combine first and last name of author
$authorFullName = $authorRow['first_name'] . " " . $authorRow['last_name'];

// Raw date and time
$dateCreated = $row['date_created'];
// Date and time object
// This will be formatted for display
$formatDateCreated = new DateTime($dateCreated);
// January 1, 2017 at 00:00AM
$dateTimeFormat = 'F j, Y \a\t g:i A';

// Display blog title, author, date posted, and entry
echo '<table><th>' . $row['title'] . '</th>';
echo '<th><div align="right">by ' . $authorFullName . '</div></th></table>';
echo '<table><tr><td>Posted ' . $formatDateCreated->format($dateTimeFormat) . '</td></tr>';
// Using nl2br to properly format the post (includes line breaks)
echo '<tr><td>' . nl2br($row['entry']) . '</td></tr></table>';

// If session ID matched blog creator id, allow for edit and delete
if ($_SESSION['user_id'] == $row['users_fk'])
{
    echo "<!-- Styling for the table-->
<link rel=\"stylesheet\" type=\"text/css\" href=\"formStyle.css\">";

    echo '
<form class="standardForm" action="" method="post">
<input class="buttonSmaller" type="submit" value="Edit Blog" name="editButton"></button>
<input class="buttonSmaller" type="submit" value="Delete Blog" name="deleteButton"></input>
</form>
';

    // Edit blog currently being viewed
    if (isset($_POST['editButton']))
    {
        $_SESSION['entry_id'] = $blogID;
        $_SESSION['entry_title'] = $row['title'];
        $_SESSION['entry_text'] = $row['entry'];
        echo nl2br('<meta http-equiv="refresh" content="0;URL=editEntry.php" />');
    }

    // Delete current blog being viewed
    if (isset($_POST['deleteButton']))
    {
        $_SESSION['entry_id'] = $blogID;
        $_SESSION['author_id'] = $row['users_fk'];
        echo nl2br('<meta http-equiv="refresh" content="0;URL=deleteEntry.php" />');
    }
}

$mysqli->close();