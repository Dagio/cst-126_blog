<?php

/*
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
Contains the footer to be placed between the footer tags of each page.
*/

echo "<br>Copyright 2017 - Grand Canyon University";