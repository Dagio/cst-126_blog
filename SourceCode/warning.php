<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.5

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 27, 2017

Synopsis:
This page will be used to display warnings to the user.  Whenever a warning is encountered, the user is redirected to
this page.  The latest warning message and previous page URL are stored in the session.  This is similar to the error
page but is to be used for normal occurrences, not errors.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Warning</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>

<?php
echo '<form method="post" class="standardForm">' . $_SESSION['warning_message'] . '
<button formaction="' . $_SESSION['previous_page'] . '" class="button">Previous Page</button></form>';
?>

</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>