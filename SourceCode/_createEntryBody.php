<?php
/*
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This file contains the blog entry form.  This is only included in createEntry.php when the user is logged in, otherwise,
a message is displayed informing the user they must be logged in to post a blog entry.
*/

// Check if editing a post
// Fill in text boxes if appropriate
if ($_SESSION['entry_mode'] == 'new')
{
    $titleText = "";
    $entryText = "";
}
//If the blog already exists, edit instead of create new
else if ($_SESSION['entry_mode'] == 'edit')
{
    $titleText = $_SESSION['entry_title'];
    $entryText = $_SESSION['entry_text'];
}
echo '
<form class="blogEntryForm" action="entryHandler.php" method="post">
    Title
    <input type="text" value="' . $titleText . '" name="title" placeholder="Blog Title" maxlength="40">
    Entry
    <textarea name="entry" cols="20" rows="20" placeholder="Blog text goes here..." maxlength="50000">' . $entryText . '</textarea>
    <input type="submit" value="Post" class="button">
</form>
';