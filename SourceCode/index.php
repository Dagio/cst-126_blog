<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.7

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 10, 2017

Synopsis:
Home page for myBlog.  Includes a list of all features that can be found on myBlog.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Home</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>
Welcome to myBlog!  Created in 2017 by Nicholas Robertson and <br>
Michael Weaver, myBlog contains the following features:
<br><br>
User Registration<br>
User Login/Logout<br>
New Blog Entries<br>
Delete/Edit Blogs<br>
List and View All Blogs<br>
Search Blogs<br>
Comment on Blogs
</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>