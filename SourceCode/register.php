<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This file contains the new user registration form.  Form data is sent to a PHP handler, where it is validated and
then written to the users database.

References:
HTNK Input Types - https://www.w3schools.com/html/html_form_input_types.asp
HTML Input Max Length - https://www.w3schools.com/tags/att_input_maxlength.asp
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - New User Registration</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>

<form class="standardForm" action="registerHandler.php" method="post">
    <h1>Name</h1>
    Display Name
        <input type="text" name="displayName" placeholder="Your display name..." maxlength="25">
    First Name
    <input type="text" name="firstName" placeholder="Your first name..." maxlength="50">
    Last Name
    <input type="text" name="lastName" placeholder="Your last name..." maxlength="50">

    <h1>Email</h1>

    Email
    <input type="email" name="email" placeholder="your@emailaddress.com" maxlength="100">
    Confirm Email
    <input type="email" name="confirmEmail" placeholder="your@emailaddress.com" maxlength="100">

    <h1>Password</h1>

    Password
    <div class="tooltip">
        <input type="password" name="password" placeholder="Password" maxlength="100">
        <span class="tooltipText">Your password must contain at least 8 characters, an uppercase, a number, and a special character.</span>
    </div>
    Confirm Password
    <div class="tooltip">
        <input type="password" name="confirmPassword" placeholder="Confirm Password" maxlength="100">
        <span class="tooltipText">Your password must contain at least 8 characters, an uppercase, a number, and a special character.</span>
    </div>
    <input type="submit" value="Create Account" class="button">
</form>

</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>