<?php

/*
Grand Canyon University
CST-126

myBlog
Version 1.5

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 27, 2017

Synopsis:
Contains the logo and menu buttons to be placed in the header of each page.
*/

echo '<img src=Images/logo.png>

<br>

<form class="menuForm">
<button formaction="index.php" class="menuButton">Home</button>
</form>

<form class="menuForm">
<button formaction="blogs.php" class="menuButton">Blogs</button>
</form>

<form class="menuForm">
<button formaction="search.php" class="menuButton">Search</button>
</form>

<form class="menuForm">
<button formaction="createEntry.php" class="menuButton">New Blog</button>
</form>
';

if (isset($_SESSION['user_id']))
{
    echo '<form class="menuForm">
<button formaction="logout.php" class="menuButton">Logout</button>
</form>';
}
else
{
    echo '<form class="menuForm">
<button formaction="login.php" class="menuButton">Login</button>
</form>';
}

echo '<br>';