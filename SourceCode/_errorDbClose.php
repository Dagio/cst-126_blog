<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This file needs to be included after any error in which a database connection is open.  This will close the connection
and halt the current script, preventing any data from being written to the database.  The error() function will
automatically redirect the user to the error page.
-->

<?php
if(is_resource($mysqli))
{
    $mysqli->close();
}
die();