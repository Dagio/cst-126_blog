<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.7

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 10, 2017

Synopsis:
The blog entry handler file processes and validates the input from the blog entry form.  Once a successful connection to
the database is established, the new blog is posted to the blogs table, recording the date/time of the posting and the
author, stored as the user's ID.

This file also doubles as a post editor.  Depending on the session variable 'entryMode', either a new blog will be
added to the database, or it will be updated.

References:
PHP Password Hashing - http://php.net/manual/en/function.password-hash.php
Sessions - https://www.youtube.com/watch?v=WuZBQ706thI
Insert Datetime in mySQL - https://stackoverflow.com/questions/9541029/insert-current-date-in-datetime-format-mysql
Prepared Statements - http://php.net/manual/en/mysqli.quickstart.prepared-statements.php
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - New Blog</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<?php
// Include PHP functions
include_once '_functions.php';

$entryMode = $_SESSION['entry_mode'];
$entryID = $_SESSION['entry_id'];

if ($entryMode == 'new')
{
    // Previous page to go to for error handling
    $previousPage = "createEntry.php";
}
else if ($entryMode == 'edit')
{
    // Previous page to go to for error handling
    $previousPage = "editEntry.php";
}

// Retrieve input from the blog entry form
$title = $_POST['title'];
$entry = $_POST['entry'];

// Check if user is logged in
// This page should only display if user_id is set, so this is here as just another safeguard
if (isset($_SESSION['user_id']))
{
    $userId = $_SESSION['user_id'];
}
else
{
    error("You must be logged in to create a blog entry.", $previousPage);
    $mysqli->close();
    return;
}

// Get current date and time
// This will be stored in table as both date_created and date_modified
$dateTime = date('Y-m-d H:i:s');

//Check if title not empty
if (empty($title)){
    error("Title cannot be left blank.", $previousPage);
    include '_errorDbClose.php';
}
//Check if body is empty
if (empty($entry)){
    error("Entry cannot be left blank." , $previousPage);
    include '_errorDbClose.php';
}

// Connect to blog database
$mysqli = dbConnect();

// Build query to either insert blog or update blog
if ($entryMode == 'new')
{
    $insertQuery = "INSERT INTO blogs (users_fk, date_created, date_modified, title, entry) VALUES ((?), (?), (?), (?), (?))";

    // Prepared SQL Statement
    if (!($preparedStatement = $mysqli -> prepare($insertQuery)))
    {
        error("Could not prepare query.", $previousPage);
        include '_errorDbClose.php';
    }

    if (!$preparedStatement -> bind_param("issss", $userId, $dateTime, $dateTime, $title, $entry))
    {
        error("Could not bind parameters.", $previousPage);
        include '_errorDbClose.php';
    }

    if (!$preparedStatement -> execute())
    {
        error("Database error:  Could not create blog entry.", $previousPage);
        include '_errorDbClose.php';
    }

    echo "Your blog has been successfully posted.";
}
else if ($entryMode == 'edit')
{
    $updateQuery = "UPDATE blogs SET date_modified = (?), title = (?), entry = (?) WHERE id = (?)";

    // Prepared SQL Statement
    if (!($preparedStatement = $mysqli -> prepare($updateQuery)))
    {
        error("Could not prepare query.", $previousPage);
        include '_errorDbClose.php';
    }

    if (!$preparedStatement -> bind_param("sssi", $dateTime, $title, $entry, $entryID))
    {
        error("Could not bind parameters.", $previousPage);
        include '_errorDbClose.php';
    }

    if (!$preparedStatement -> execute())
    {
        error("Database error:  Could not edit blog entry.", $previousPage);
        include '_errorDbClose.php';
    }

    echo "Your blog has been successfully updated.  Redirecting back to blog entry...";
    echo nl2br('<meta http-equiv="refresh" content="2;URL=blogDisplay.php?blogID=' . $_SESSION['entry_id'] . '" />');
}

$mysqli->close();
?>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>
