<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.7

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 10, 2017

Synopsis:
This file contains the form for posting a new blog entry.  The session is checked to confirm that a user is logged in.
Otherwise, they are unable to make a new blog.

References:
Textarea - https://stackoverflow.com/questions/6262472/multiple-lines-of-input-in-input-type-text
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">
<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="linkStyle.css">
<!-- Styling for the table-->
<link rel="stylesheet" type="text/css" href="tableStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Create Blog Entry</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>

<?php
if (isset($_SESSION['user_id']))
{
    $_SESSION['entry_mode'] = 'new';
    include '_createEntryBody.php';
}
else
{
    echo 'You must be <a href=\'login.php\'>logged in</a> to create a blog entry.';
}
?>
</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>