<?php
include '_startSession.php'; ?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.7

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 10, 2017

Synopsis:
This page creates queries based on the input from the search.php form and then passes variables to the searchResults.php
page.

References:
Concatenate MYSQL Tables - https://stackoverflow.com/questions/3251600/how-do-i-get-first-name-and-last-name-as-whole-name-in-a-mysql-query
Boolean Mode - https://dev.mysql.com/doc/refman/5.5/en/fulltext-boolean.html
-->

<?php

include_once '_functions.php';

echo '
<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="linkStyle.css">
';

$mysqli = dbConnect();

$previousPage = "search.php";

$searchPattern = $_POST['searchPattern'];
$match = $_POST['Match'];
$type = $_POST['Type'];

// Check to make sure search text is not empty
if (empty($searchPattern)){
    error("Search cannot be left blank.", $previousPage);
    include '_errorDbClose.php';
}

// Columns to be queried.  Using a variable to help readability.
$selectedColumns = 'blogs.id, blogs.users_fk, blogs.date_created, blogs.title, users.id, users.first_name, users.last_name';

// "Contains" search
// Add wildcard to search pattern
if ($match == "Contains")
{
    $searchPattern = "$searchPattern*";
}
// "Exact" search
// Add quotes around search pattern
else
{
    $searchPattern = "\"$searchPattern\"";
}
// Add single quotes around entire search pattern
// This is needed for the AGAINST statements below
$searchPattern = "'" . $searchPattern . "'";

// Build query based on search type (author, title, entry)
switch ($type)
{
    case "Title":
        $searchQuery = "SELECT $selectedColumns FROM blogs, users WHERE blogs.users_fk=users.id AND MATCH(blogs.title) AGAINST($searchPattern IN BOOLEAN MODE) ORDER BY blogs.id DESC";
        break;
    case "Entry":
        $searchQuery = "SELECT $selectedColumns FROM blogs, users WHERE blogs.users_fk=users.id AND MATCH(blogs.entry) AGAINST($searchPattern IN BOOLEAN MODE) ORDER BY blogs.id DESC";
        break;
    case "AuthorFirst":
        $searchQuery = "SELECT $selectedColumns FROM blogs, users WHERE blogs.users_fk=users.id AND MATCH(users.first_name) AGAINST($searchPattern IN BOOLEAN MODE) ORDER BY blogs.id DESC";
        break;
    case "AuthorLast":
        $searchQuery = "SELECT $selectedColumns FROM blogs, users WHERE blogs.users_fk=users.id AND MATCH(users.last_name) AGAINST($searchPattern IN BOOLEAN MODE) ORDER BY blogs.id DESC";
        break;
    default:
        $searchQuery = "";
}

$_SESSION['search_query'] = $searchQuery;
$_SESSION['search_pattern'] = $searchPattern;
echo nl2br('<meta http-equiv="refresh" content="0;URL=searchResults.php" />');