<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.7

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 10, 2017

Synopsis:
This page utilizes the blogs handler to display search results.  By separating this from blogs.php, we are able to
reset the search session variables when navigating away from this page.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Search Results</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>
<?php
// Search - set session variable
$_SESSION['is_search'] = true;
include_once '_blogsHandler.php';
?>

</body>
<footer>
    <?php
    include '_footer.php';
    ?>
</footer>
</html>