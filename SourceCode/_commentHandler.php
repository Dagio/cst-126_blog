<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.6

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 3, 2017

Synopsis:
Handler file for displaying comments.  To be included on each blog entry display.

References:

-->

<?php

echo "<br>";
include_once '_functions.php';

$blog_fk = $_GET['blogID'];

echo '
<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="linkStyle.css">
';
echo '<!-- Styling for the table-->
<link rel="stylesheet" type="text/css" href="tableStyle.css">
';

$mysqli = dbConnect();

//I feel this should act as a dropdown box that displays or hides all comments
echo'<br><b>Comments</b>';

/*
<input type="checkbox" name="viewComments" value="view"> View Comments
if ($_POST['viewComments'] == 'view'){
    echo "here are your comments";
}
*/

// Inner join between comments and users
// Get each comment's author's first and last name and order comments starting with the oldest
$getCommentsQuery = "SELECT * FROM comments, users WHERE comments.users_fk=users.id AND comments.blogs_fk=$blog_fk ORDER BY comments.id ASC";

$result = mysqli_query($mysqli, $getCommentsQuery);

echo '<table>';
if (mysqli_num_rows($result) == 0)
{
    echo "<tr><td>No comments to display.</td></tr>";
}
while ($row = mysqli_fetch_array($result)){
    $comment = $row['message'];
    $author = $row['first_name'] . " " . $row['last_name'];

    // Raw date and time
    $dateCreated = $row['date_created'];
    // Date and time object
    // This will be formatted for display
    $dateTime = new DateTime($dateCreated);

    // January 1, 2017 at 00:00AM
    $dateTimeFormat = 'n/j/Y \a\t g:i A';

    echo"<tr>";
    echo"
<td>" . nl2br($comment) . "<br>
<i>$author, " . $dateTime->format($dateTimeFormat) . "</i></td>";
    echo"</tr>";
}
echo '</table>';

// $authorID is set in _displayHandler.php
// This variable equals the author of the blog entry
// Prevent author from posting comments on his/her own blog
if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != $authorID)
{
    // Set blog ID in session to pass to create comment handler
    $_SESSION['$comments_blogs_fk'] = $blog_fk;
    echo '<br>
<form class="standardForm" action="_createComment.php" method="post">
<b>Post a Comment</b>
<textarea name="comment" cols="1" rows="5" placeholder="Enter your comment..." maxlength="500"></textarea>
<input class="buttonSmaller" type="submit" value="Post" class="button">
</form>';
}
// If logged in user = blog author
else if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == $authorID)
{
    echo '<br>You cannot comment on your own blog.';
}
// If guest user
else
{
    echo '<br><br>You must be <a href=\'login.php\'>logged in</a> to post a comment.';
}

?>
