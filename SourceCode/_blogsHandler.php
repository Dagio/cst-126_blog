<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.7

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 10, 2017

Synopsis:
blogsHandler is the call for the main blogs page. This queries all of the active blogs in the database and then
lists them for anyone to view. There are currently no restrictions on who can view the blogs.

References:
Format Date/Time - https://stackoverflow.com/questions/24473092/how-can-i-convert-the-date-format-from-html5-php
Substrings - http://php.net/manual/en/function.substr.php
String Length - http://php.net/manual/en/function.strlen.php
Auto-Submit DropDown List - https://stackoverflow.com/questions/7231157/how-to-submit-form-on-change-of-dropdown-list
-->
<?php
include_once '_functions.php';

echo '
<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="linkStyle.css">
';

$previousPage = 'index.php';

$mysqli = dbConnect();

$searchPattern = $_SESSION['search_pattern'];

if ($searchPattern === null || $searchPattern == ""){
    // Perform inner join between "blogs" and "users" tables
    // Foreign key in blogs table is the ID column from users table
    $blogsQuery = "SELECT * FROM blogs, users WHERE blogs.users_fk=users.id ORDER BY blogs.id DESC";
}
else
{
    $blogsQuery = $_SESSION['search_query'];
}

$blogsRecords = mysqli_query($mysqli, $blogsQuery);

echo "<!-- Styling for the table-->
<link rel=\"stylesheet\" type=\"text/css\" href=\"tableStyle.css\">";

// Count number of blogs
$totalEntries = mysqli_num_rows($blogsRecords);

if ($_SESSION['is_search'] == true)
{
    if ($totalEntries > 100)
    {
        warning("Too many results to display.  Please narrow your search.", "search.php");
    }

    if ($totalEntries == 0)
    {
        warning("No results found.  Please try again.", "search.php");
    }
}

// Determine number of pages to display blogs
$blogsPerPage = 10;
$totalPages = $totalEntries / $blogsPerPage;
// Need to account for missing blog(s) by adding 1 more page
// This last page will have less than the number of blogs per page
if ($totalEntries % $blogsPerPage != 0)
{
    $totalPages += 1;
}

$showBlogs = $_GET['showBlogs'];

// Check if $showBlogs is a valid number
if ($showBlogs != null && is_numeric($showBlogs))
{
    // Get integer value from $showBlogs
    // This is to prevent the user from manipulating values in the URL (entering showBlogs=1.5 for example)
    $currentPage = intval($showBlogs);
}
else
{
    $currentPage = 1;
}

$blogNumberHigh = $blogsPerPage * $currentPage;
$blogNumberLow = $blogNumberHigh - $blogsPerPage + 1;

if ($totalEntries == 0)
{
    $blogNumberHigh = 0;
    $blogNumberLow = 0;
}

if ($blogNumberHigh > $totalEntries)
{
    $blogNumberHigh = $totalEntries;
}

// Number of blogs currently being shown
echo '<table><tr><td>Showing entries ' . $blogNumberLow . '-' . $blogNumberHigh . " of " . $totalEntries . '</td>';

// Build dynamic dropdown list for page numbers
echo '<td></td><td><div align="right">Browse to: 
<form class="menuForm" method="GET">
    <select class="dropDown" name="showBlogs" onChange="this.form.submit()">
    ';

for ($i = 1; $i <= $totalPages; $i++)
{
    echo '<option value="' . $i . '"';

    // In the dropdown list, select the current page number
    // Without this, page 1 would be selected by default, preventing user from selecting page 1
    if ($i == $currentPage)
    {
        echo 'selected="selected"';
    }

    echo '>Page ' . $i . '</option>';
}

echo '
</select>
</td>
</div>
</tr>
</form>
';

// List of blogs table
echo "";
echo "<tr>";
echo "<th width='33%'> Title </th>";
echo "<th width='33%'> Author </th>";
echo "<th width='34%'> Date Posted </th>";
echo "</tr>";

// Display list of blogs
// Only display limited number of blogs, based on $blogsPerPage
$counter = 1;
while ($row = mysqli_fetch_array($blogsRecords))
{
    // Skip over blogs if they're not supposed to be on this page
    if ($counter < $blogNumberLow)
    {
        $counter++;
        continue;
    }

    $title = $row['title'];
    $author = $row['first_name'] . ' ' . $row['last_name'];
    $blogID = $row[0];

    // Check for long title
    // If title is more than 30 characters, crop title after 30 characters and add "..."
    if (strlen($title) > 30)
    {
        $title = substr($title, 0, 30);
        $title = $title . "...";
    }

    // Raw date and time
    $dateCreated = $row[2];
    // Date and time object
    // This will be formatted for display
    $formatDateCreated = new DateTime($dateCreated);

    // January 1, 2017 at 00:00AM
    $dateTimeFormat = 'F j, Y \a\t g:i A';

    echo "<tr>";
    echo "<td> <a href='blogDisplay.php?blogID=$blogID'>{$title}</a> </td>";
    echo "<td> {$author} </td>";
    echo '<td>'. $formatDateCreated->format($dateTimeFormat) . '</td>';
    echo "</tr>";

    $counter++;

    // If reached the max number of blogs, end loop and do not display any more blogs
    if ($counter > $blogNumberHigh)
    {
        break;
    }
}

echo '</table>';

// PHP page fragment
// Needs its own call to .css file
echo '<link rel="stylesheet" type="text/css" href="formStyle.css">';

echo '<div id="leftColumn" align="left">';
if ($currentPage > 1)
{
    // Regular blogs list
    if ($_SESSION['is_search'] == false)
    {
        echo '
<form class="menuForm">
<button formaction="blogs.php" name="showBlogs" class="buttonSmaller" method="GET" value="' . ($currentPage - 1) . '">Previous</button>
</form>
';
    }
    // Filtered blogs list (search)
    else
    {
        echo '
<form class="menuForm">
<button formaction="searchResults.php" name="showBlogs" class="buttonSmaller" method="GET" value="' . ($currentPage - 1) . '">Previous</button>
</form>
';
    }
}
echo '</div>';

if ($currentPage < $totalPages - 1)
{

    // Regular blogs list
    if ($_SESSION['is_search'] == false)
    {
        echo '
<div id="rightColumn" align="right">
<form class="menuForm">
<button formaction="blogs.php" name="showBlogs" class="buttonSmaller" method="GET" value="' . ($currentPage + 1) . '">Next</button>
</form>
</div>
';
    }
    else
    {
        echo '
<div id="rightColumn" align="right">
<form class="menuForm">
<button formaction="searchResults.php" name="showBlogs" class="buttonSmaller" method="GET" value="' . ($currentPage + 1) . '">Next</button>
</form>
</div>
';
    }
}

$mysqli -> close();
?>