<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
Blogs Page containing a list of all blogs. Currently the page calls the handler file which then
populates the list of available blogs.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - View Blogs</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>
<?php
// Not a search - clear all search session variables
$_SESSION['is_search'] = false;
$_SESSION['search_query'] = "";
$_SESSION['search_pattern'] = "";
include_once '_blogsHandler.php';
?>

</body>
<footer>
    <?php
    include '_footer.php';
    ?>
</footer>
</html>