<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.5

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 27, 2017

Synopsis:
This file processes form data sent from the user registration page.  Each input is validated, checking for, if
applicable, minimum length, maximum length, and/or the types of characters.

References:
Check Special Characters - https://stackoverflow.com/questions/8053837/preg-match-for-all-special-characters-password-checking
PHP Password Hashing - http://php.net/manual/en/function.password-hash.php
Prepared Statements - http://php.net/manual/en/mysqli.quickstart.prepared-statements.php
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Registering user...</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<?php
// Include PHP functions
include_once '_functions.php';

// Previous page to go to for error handling
$previousPage = "register.php";

// Obtain Post variables
$displayName = $_POST['displayName'];
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$email = $_POST['email'];
$confirmEmail = $_POST['confirmEmail'];
$password = addslashes($_POST['password']);
$confirmPassword = addslashes($_POST['confirmPassword']);

// Validate form data before writing to database.
// Although the HTML input fields have maxLength values assigned, character lengths will still be checked as a final safeguard.
// Each maxLength matches the Length of the column in the users table.

// Check display name requirements
if (empty($displayName))
{
    error("Display name cannot be blank.", $previousPage);
    include '_errorDbClose.php';
}
if (strlen($displayName) > 25)
{
    error("Display name cannot exceed 25 characters.", $previousPage);
    include '_errorDbClose.php';
}

// Check first name requirements
if (empty($firstName))
{
    error("First name cannot be blank.", $previousPage);
    include '_errorDbClose.php';
}
if (strlen($firstName) > 50)
{
    error("First name cannot exceed 50 characters.", $previousPage);
    include '_errorDbClose.php';
}

// Check last name requirements
if (empty($lastName))
{
    error("Last name cannot be blank.", $previousPage);
    include '_errorDbClose.php';
}
if (strlen($lastName) > 50)
{
    error("Last name cannot exceed 50 characters.", $previousPage);
    include '_errorDbClose.php';
}

// Check email requirements
if ($email != $confirmEmail)
{
    error("Email confirmation does not match.", $previousPage);
    include '_errorDbClose.php';
}

if (empty($email))
{
    error("Email cannot be blank.", $previousPage);
    include '_errorDbClose.php';
}

if (strlen($email) > 100)
{
    error("Email cannot exceed 100 characters.", $previousPage);
    include '_errorDbClose.php';
}

// Check password requirements
if ($password != $confirmPassword)
{
    error("Password confirmation does not match.", $previousPage);
    include '_errorDbClose.php';
}
if (strlen($password) < 8)
{
    error("Password must contain at least 8 characters.", $previousPage);
    include '_errorDbClose.php';
}
if (!preg_match('/[A-Z]/', $password))
{
    error("Password must contain at least one uppercase letter.", $previousPage);
    include '_errorDbClose.php';
}
if (!preg_match('/[0-9]/', $password))
{
    error("Password must contain at least one number.", $previousPage);
    include '_errorDbClose.php';
}
// Check for at least one special character in password
$specialCharacters = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
if (!preg_match($specialCharacters, $password))
{
    error("Password must contain at least one special character.", $previousPage);
    include '_errorDbClose.php';
}

// Generate hash for password
// This will be the value stored in the database
$passwordHash = password_hash($password, PASSWORD_DEFAULT);

// Connect to blog database
$mysqli = dbConnect();

// Check if e-mail account is already in database
$whereQuery = "SELECT * FROM users WHERE email='$email'";
$result = $mysqli->query($whereQuery);
if ($result === FALSE)
{
    error(nl2br("Database error:  Query unsuccessful."), $previousPage);
    include '_errorDbClose.php';
}
if ($result->num_rows > 0)
{
    error(nl2br("A user is already registered with the email address:\n\n" . $email . "\n\nPlease try a different email address."), $previousPage);
    include '_errorDbClose.php';
}

$insertQuery = "INSERT INTO users (display_name, first_name, last_name, email, password) VALUES ((?), (?), (?), (?), (?))";

// Prepared SQL Statement
if (!($preparedStatement = $mysqli -> prepare($insertQuery)))
{
    error("Could not prepare query.", $previousPage);
    include '_errorDbClose.php';
}

if (!$preparedStatement -> bind_param("sssss", $displayName, $firstName, $lastName, $email, $passwordHash))
{
    error("Could not bind parameters.", $previousPage);
    include '_errorDbClose.php';
}

if (!$preparedStatement -> execute())
{
    error("Database error:  Could not create new user.", $previousPage);
    include '_errorDbClose.php';
}

$mysqli->close();

?>

<?php
echo nl2br("Success! Here is your account information:" . "\n");
echo nl2br("First Name: " . $firstName . "\n");
echo nl2br("Last Name: " . $lastName . "\n");
echo nl2br("Email: " . $email . "\n");

?>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>