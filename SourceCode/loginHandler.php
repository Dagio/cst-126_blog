<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
The handler file processes and validates the user input from the main form. The input is checked against the database
and only if the email and password match existing entries is the user logged in.

References:
PHP Password Hashing - http://php.net/manual/en/function.password-hash.php
Sessions - https://www.youtube.com/watch?v=WuZBQ706thI
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Logging in...</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<?php
// Include PHP functions
include_once '_functions.php';

// Previous page to go to for error handling
$previousPage = "login.php";

//Retrieve input from the HTML form
$email = $_POST['email'];
$password = $_POST['password'];

//Check to make sure email is not empty
if (empty($email)){
    error("Email cannot be left blank.", $previousPage);
    include '_errorDbClose.php';
}
//Check to make sure that password is not empty
if (empty($password)){
    error("Password cannot be left blank.", $previousPage);
    include '_errorDbClose.php';
}

// Connect to blog database
$mysqli = dbConnect();

//Reference to check password based on email entry
$passwordQuery = "SELECT id, password, display_name FROM users WHERE email = '$email'";

$result = $mysqli->query($passwordQuery);

//Throw error and exit if no email found
if ($result->num_rows == 0)
{
    error("Error: User not found.", $previousPage);
    include '_errorDbClose.php';
}
//Throw error and exit if identical emails found
if ($result->num_rows > 1)
{
    error("Error: Multiple users with same email found.", $previousPage);
    include '_errorDbClose.php';
}
//Get associated password
$row = $result->fetch_assoc();
//Hash associated password
$hash = $row["password"];
//Verify password and exit if they do not match
if (!password_verify($password, $hash))
{
    error("Invalid Password", $previousPage);
    include '_errorDbClose.php';
}

// Store the logged in user's ID
$_SESSION['user_id'] = $row["id"];

$displayName = $row["display_name"];

$mysqli->close();

echo nl2br("Welcome, " . $displayName . ".  Redirecting to the home page...\n");
echo nl2br('<meta http-equiv="refresh" content="2;URL=index.php" />');
?>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>
