<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.5

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 27, 2017

Synopsis:
Search page, which allows the user to search for blogs by filtering the blog title or author.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Search</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>

<form class="standardForm" action="_searchHandler.php" method="POST">
    <b>Search</b><br>
    <input type="text" name="searchPattern" placeholder="Search text..." maxlength="50">
    <table>
        <tr>
            <th align="left"><b>Type</b></th>
            <th align="left"><b>Match</b></th>
        </tr>
        <tr>
            <td>
                <input type="radio" name="Type" checked="true" value="Title">Title
            </td>
            <td>
                <input type="radio" name="Match" checked="true" value="Contains">Contains
            </td>
        </tr>
        <tr>
            <td>
                <input type="radio" name="Type" value="Entry">Entry
            </td>
            <td>
                <input type="radio" name="Match" value="Exact">Exact
            </td>
        </tr>
        <tr>
            <td>
                <input type="radio" name="Type" value="AuthorFirst">Author First Name
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <input type="radio" name="Type" value="AuthorLast">Author Last Name
            </td>
            <td>
            </td>
        </tr>
    </table><br>
    <input type="submit" value="Search" class="button">
</form>

</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>