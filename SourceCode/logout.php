<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This file clears the user ID in the session, then auto-redirects the user to the login page.

References:
Auto-redirect - https://stackoverflow.com/questions/14031569/redirecting-a-page-automatically-in-php
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Logging out...</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<?php
unset($_SESSION['user_id']);
?>

<body>
<meta http-equiv="refresh" content="1;URL=login.php" />
Logging out...
</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>