<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This file contains the form for editing an existing blog entry.  The session is checked to confirm that a user is
logged in.  Otherwise, they are unable to edit the blog.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Edit Blog</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>

<?php
if (isset($_SESSION['user_id']))
{
    $_SESSION['entry_mode'] = 'edit';
    include '_createEntryBody.php';
}
else
{
    echo 'You must be logged in to create a blog entry.';
}
?>
</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>