<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This page will be used to display errors to the user.  Whenever an error is encountered, the user is redirected to this
page.  The latest error message and previous page URL are stored in the session.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Error Encountered</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>

<?php
echo '<form method="post" class="standardForm"><h1>Error</h1> ' . $_SESSION['error_message'] . '
<button formaction="' . $_SESSION['previous_page'] . '" class="button">Previous Page</button></form>';
?>

</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>