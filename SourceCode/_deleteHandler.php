<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.7

Authors:
Nicholas Robertson
Michael Weaver

Date:
September 10, 2017

Synopsis:
This file handles the deletion of a blog.  Deleting the blog also deletes all comments associated with the blog post.
-->

<?php
include_once '_functions.php';

// Previous page to go to for error handling
$previousPage = "blogDisplay.php?blogID=" . $_SESSION['entry_id'];
//Ensure that the current user ID is the author ID
if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == $_SESSION['author_id'])
{
    echo 'Are you sure you want to delete your blog entry and all comments?';
    echo '
<form action="" method="POST">
<input class="buttonSmaller" type="submit" value="Yes" name="yesButton"></input>
<input class="buttonSmaller" type="submit" value="No" name="noButton"></input>
</form>
    ';

    // Edit blog currently being viewed
    if (isset($_POST['yesButton']))
    {
        // Set up database connection
        $mysqli = dbConnect();

        //Query to delete all comments associated with the blog ID
        $query = "DELETE FROM comments WHERE blogs_fk=" . $_SESSION['entry_id'];

        // Run query
        if (!mysqli_query($mysqli, $query))
        {
            error("Database error:  Could not delete comments associated with blog entry.", $previousPage);
            include '_errorDbClose.php';
        }

        // Query to delete the blogs row associated with the blog ID
        $query = "DELETE FROM blogs WHERE id=" . $_SESSION['entry_id'];

        // Run query
        if (!mysqli_query($mysqli, $query))
        {
            error("Database error:  Could not delete blog entry.", $previousPage);
            include '_errorDbClose.php';
        }

        $_SESSION['view_message'] = "Your blog and all comments have been deleted.";
        echo nl2br('<meta http-equiv="refresh" content="0;URL=viewMessage.php" />');

        $mysqli->close();
    }

    // User clicks no - go back to blog display
    if (isset($_POST['noButton']))
    {
        echo nl2br('<meta http-equiv="refresh" content="0;URL='. $previousPage . '" />');
    }
}
// User not logged in
else
{
    error('You must be logged in to delete your blog entry.', $previousPage);
}
