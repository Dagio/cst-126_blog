<?php
include '_startSession.php';
?>

<!--
Grand Canyon University
CST-126

myBlog
Version 1.4

Authors:
Nicholas Robertson
Michael Weaver

Date:
August 20, 2017

Synopsis:
This page will be used to display a message to the user, such as successfully deleting a blog.
-->

<!DOCTYPE html>
<html lang="en">

<!-- Link referencing global format for webpages -->
<link rel="stylesheet" type="text/css" href="formStyle.css">

<head>
    <meta charset="UTF-8">
    <title>myBlog - Message</title>
</head>

<header>
    <?php include '_header.php'; ?>
</header>

<body>
<?php
echo $_SESSION['view_message'];
?>
</body>

<footer>
    <?php
    include '_footer.php';
    ?>
</footer>

</html>