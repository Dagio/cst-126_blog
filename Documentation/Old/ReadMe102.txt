Grand Canyon University
CST - 126

Untitled Blog Project
Version 1.2

Authors:
Michael Weaver
Nicholas Robertson

Date
8/6/17

Week 1 Synopsis:

For our blog project we have created a Login page which enables a user to enter their email address and password, which is checked against the database to determine if the user has valid login information.

For new users we have also created a register page which allows new users to register for access to the blog. Thier information is validated by the PHP handler and then added to the database as long as all criteria are met.

As of version 1.1 no official name for the blog has been chosen.

The blog folder is the database, which must be placed in your local MAMP/db/mysql folder.

Week 2 Synopisis:

We have created a header function which includes a navigation menu for our myBlog project. Additionally a number of repeated blocks of php code have been transformed into functions in an attempt to increase code functionality and readability.

The page now stores the current logged in user and based upon this the active users blogs are populated. Addiitonally only active uers may create or view existing blogs.

In order to account for this added functionality we have created a new blog.php page, handler, as well as createEntry.php pages.

Included Files:

ReadMe.txt

.idea Folder
blog Folder (Database)
Documentation Folder
-Images Folder
-User Guides Folder
--User Guide : Creating a new Blog.docx
--User Guide : The Basics.docx
-Code Implementation.docx
-ReadMe101.txt
-ReadMe102.txt
-Schema.graphml
-Schema.png
Images Folder (Image Logo)

_createEntryBody.php
_footer.php
_functions.php
_header.php
blogs.php
blogsHandler.php
createEntry.php
entryHandler.php
formStyle.css
index.php
login.php
loginHandler.php
logout.php
README.md
register.php
registerHandler.php


