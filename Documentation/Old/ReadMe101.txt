Grand Canyon University
CST - 126

Untitled Blog Project
Version 1.1

Authors:
Michael Weaver
Nicholas Robertson

Date
7/30/17

Synopsis:
For our blog project we have created a Login page which enables a user to enter their email address and password, which is checked against the database to determine if the user has valid login information.

For new users we have also created a register page which allows new users to register for access to the blog. Thier information is validated by the PHP handler and then added to the database as long as all criteria are met.

As of version 1.1 no official name for the blog has been chosen.

The blog folder is the database, which must be placed in your local MAMP/db/mysql folder.

Included Files:
ReadMe.txt
.idea Folder
formStyle.css
login.html
loginHandler.php
register.html
registerHandler.php
User Guide.docx
Schema.png
Schema.graphml
Images Folder
blog Folder

