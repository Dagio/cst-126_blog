Grand Canyon University
CST - 126

myBlog
Version 1.7

Authors:
Michael Weaver
Nicholas Robertson

Date
9/10/17

Week 1 Synopsis:

For our blog project we have created a Login page which enables a user to enter their email address and password, which is checked against the database to determine if the user has valid login information.

For new users we have also created a register page which allows new users to register for access to the blog. Thier information is validated by the PHP handler and then added to the database as long as all criteria are met.

As of version 1.1 no official name for the blog has been chosen.

The blog folder is the database, which must be placed in your local MAMP/db/mysql folder.

Week 2 Synopisis:

We have created a header function which includes a navigation menu for our myBlog project. Additionally a number of repeated blocks of php code have been transformed into functions in an attempt to increase code functionality and readability.

The page now stores the current logged in user and based upon this the active users blogs are populated. Addiitonally only active uers may create or view existing blogs.

In order to account for this added functionality we have created a new blog.php page, handler, as well as createEntry.php pages.

Week 3 Synopsis:

This week we have focused on reworking the code and documentation to be cleaner.

Instead of submitting numerous documentation files a single Design Report contains all the necessary submission material and documented changes.

Week 4 Synopsis:

This week we have formatted the blog display list so that it contains a table of available blogs. Each blog is displayed in a list, and with enough blogs In the database multiple pages can be displayed.

Each blog contains a hyperlink to display the blog content. The entry, creation date, and author are all publicly displayed.

If the user ID matches the author ID then the additional capability of Editing and Deleting the blog appears.

If the edit button is executed then the page retrieves the entry and blog title from the database and populates it into a textbox and textarea for the user to edit. The changes can then be saved to the database.

If the delete button is executed then the page redirects the user to a new page where they are prompted to confirm their choice. If they proceed then the active blog is deleted.

Week 5 Synopsis:

This week we implemented a search feature, allowing the user to search for blog posts via the title of the blog, the text of the blog, or the author's first or last name.

Each column that can be used as search criteria has been indexed using FULLTEXT, with the query using MATCH() AGAINST(), as opposed to LIKE or =.  This approach will dramatically improve search performance as the number of blogs grows.

Search results are limited to no more than 100 results.  If the number of returned blogs exceeds 100, a warning is displayed to the user that they must narrow their search.

Week 6 Synopsis:

This week we added Comment functionality for all blogs. A user can post a comment which is displayed below the actively viewed blog.

Commenting can only be done if the active blog is not one that you own. Commenting is also only available for users that have registered. Finally a comment will only be posted if it is not blank.

Week 7 Synopsis:

For the final week of class we cleaned up the code in the project and created a short video presentation which discusses the functionality and code of the project.

Included Files:

ReadMe.txt

.idea Folder

Documentation Folder
- Blog Folder
- DDL Script
- Old Folder
-- User Guides Folder
--- User Guide : Creating a new Blog.docx
--- User Guide : The Basics.docx
-- Code Implementation.docx
-- ReadMe101.txt
-- ReadMe102.txt
-- Schema.graphml
-- Schema.png
- Wireframe
-- BlogEntry (graphml and png)
-- Blogs (graphml and png)
-- BlogView (graphml and png)
-- Login (graphml and png)
-- Main (graphml and png)
-- Register (graphml and png)
-- SiteMap (graphml and png)
- Topic 4 Design Report.docx
Public Download Folder
- DDL Script.zip
- SourceCode.zip


SourceCode Folder:

Images Folder (Logo Image)
_blogsHandler.php
_commentHandler.php
_createComment.php
_createEntryBody.php
_deleteHandler
_displayHandler
_errorDbClose
_footer.php
_functions.php
_header.php
_searchHandler.php
_sessionStart
blogsDisplay
blogs.php
createEntry.php
deleteEntry
editEntry
entryHandler.php
error
formStyle.css
index.php
linkStyle.css
login.php
loginHandler.php
logout.php
register.php
registerHandler.php
search.php
searchResults.php
tableStyle.css
viewMessage.php
warning.php
